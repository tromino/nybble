package common

type Conf struct {
	Port uint16 `json:"port"`
	ReadTimeout int64 `json:"readTimeout"`
	WriteTimeout int64 `json:"writeTimeout"`
	SiteName string `json:"siteName"`
	SiteRoot string `json:"siteRoot"`
	MediaRoot string `json:"mediaRoot"`
	DatabaseUser string `json:"databaseUser"`
	DatabasePassword string `json:"databasePassword"`
	DatabaseDbname string `json:"databaseDbname"`
	DatabaseHost string `json:"databaseHost"`
	DatabasePort uint16 `json:"databasePort"`
	DatabaseSslmode string `json:"databaseSslmode"`
}
