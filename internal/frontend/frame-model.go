package frontend

import (
	"codeberg.org/tromino/nybble/internal/frontend/lang"
	"codeberg.org/tromino/nybble/internal/common"
)

type FrameModel struct {
	Lang lang.Lang
	Conf common.Conf
	LoggedIn bool
	Account UserInfo
}
