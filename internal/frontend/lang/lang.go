package lang

type Lang struct {
	LCode string
	LDirection string
	Date string
	Time string
	DateAtTime string
	PageHome string
	PageNew string
	PagePopular string
	PageLocal string
	Search string
	LogIn string
	SignUp string
	MainMenu string
	Upload string
	Notifications string
	Account string
	MoreFromUser string
	CommentsNum string
	WriteAComment string
	TrackByUser string
	PublishedOnDate string
	PostComment string
}
