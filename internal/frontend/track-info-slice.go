package frontend

import (
	"codeberg.org/tromino/nybble/internal/common"
	"codeberg.org/tromino/nybble/internal/backend"
)

type TrackInfoSlice []TrackInfo

func (self TrackInfoSlice) ReadApiTrackSlice(
	conf common.Conf,
	users UserInfoMap,
	reqTracks []backend.ApiTrack,
) TrackInfoSlice {
	for _, reqTrack := range reqTracks {
		self = append(self, TrackInfo {}.ReadApiTrack(conf, users, reqTrack))
	}
	
	return self
}
