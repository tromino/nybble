package frontend

import (
	"codeberg.org/tromino/nybble/internal/common"
	"codeberg.org/tromino/nybble/internal/backend"
)

type UserInfoMap map[string]UserInfo

func (self UserInfoMap) ReadApiUserSlice(
	conf common.Conf,
	reqUsers []backend.ApiUser,
) UserInfoMap {
	for _, reqUser := range reqUsers {
		self[reqUser.Id] = UserInfo {}.ReadApiUser(conf, reqUser)
	}
	
	return self
}
