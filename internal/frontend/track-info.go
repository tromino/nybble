package frontend

import (
	"time"
	"codeberg.org/tromino/nybble/internal/common"
	"codeberg.org/tromino/nybble/internal/backend"
)

type TrackInfo struct {
	Title string
	Description string
	Url string
	ThumbnailUrl string
	AudioUrl string
	Author UserInfo
	CreationTime time.Time
}

func (self TrackInfo) ReadApiTrack(
	conf common.Conf,
	users UserInfoMap,
	reqTrack backend.ApiTrack,
) TrackInfo {
	self.Title = reqTrack.Title
	self.Description = reqTrack.Description.String
	self.Url = conf.SiteRoot + "/t/" + reqTrack.Id
	self.ThumbnailUrl = reqTrack.ThumbnailUrl.String
	self.AudioUrl = reqTrack.AudioUrl
	self.Author = users[reqTrack.Author]
	self.CreationTime = time.Unix(reqTrack.CreationTime, 0)
	
	return self
}
