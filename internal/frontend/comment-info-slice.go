package frontend

import (
	"codeberg.org/tromino/nybble/internal/common"
	"codeberg.org/tromino/nybble/internal/backend"
)

type CommentInfoSlice []CommentInfo

func (self CommentInfoSlice) ReadApiCommentSlice(
	conf common.Conf,
	users UserInfoMap,
	reqComments []backend.ApiComment,
) CommentInfoSlice {
	for _, reqComment := range reqComments {
		self = append(self, CommentInfo {}.ReadApiComment(conf, users, reqComment))
	}
	
	return self
}
