package frontend

import (
	"time"
	"codeberg.org/tromino/nybble/internal/common"
	"codeberg.org/tromino/nybble/internal/backend"
)

type UserInfo struct {
	DisplayName string
	Description string
	Url string
	AvatarUrl string
	HeaderUrl string
	CreationTime time.Time
}

func (self UserInfo) ReadApiUser(
	conf common.Conf,
	reqUser backend.ApiUser,
) UserInfo {
	self.DisplayName = reqUser.DisplayName.String
	self.Description = reqUser.Description.String
	self.Url = conf.SiteRoot + "/u/" + reqUser.UniqueName
	self.AvatarUrl = reqUser.AvatarUrl.String
	self.HeaderUrl = reqUser.HeaderUrl.String
	self.CreationTime = time.Unix(reqUser.CreationTime, 0)
	
	return self
}
