package frontend

import (
	"log"
	"os"
	"time"
	"strings"
	"strconv"
	"io/fs"
	"net/http"
	"html/template"
	"codeberg.org/tromino/nybble/internal/common"
	"codeberg.org/tromino/nybble/internal/backend"
	"codeberg.org/tromino/nybble/internal/frontend/lang"
)

type Server struct {
	logs *log.Logger
	conf common.Conf
	defaultLang lang.Lang
	resources fs.FS
	server http.Server
	database *backend.Database
}

func NewServer(
	conf common.Conf,
	resources fs.FS,
) *Server {
	serveMux := http.NewServeMux()
	logs := log.New(os.Stdout, "", log.LstdFlags)
	
	server := &Server {
		logs: logs,
		conf: conf,
		defaultLang: lang.EnUs,
		resources: resources,
		server: http.Server {
			Addr: ":" + strconv.FormatUint(uint64(conf.Port), 10),
			Handler: serveMux,
			ReadTimeout: time.Millisecond * time.Duration(conf.ReadTimeout),
			WriteTimeout: time.Millisecond * time.Duration(conf.WriteTimeout),
		},
		database: backend.NewDatabase(conf, resources, logs),
	}
	
	serveMux.Handle(
		"/static/",
		http.FileServer(http.FS(server.resources)),
	)
	
	serveMux.HandleFunc("/", server.handleHome)
	serveMux.HandleFunc("/t/", server.handleTrack)
	
	serveMux.Handle("/api/", server.database.ApiHandler)
	
	return server
}

func (self *Server) Run() {
	self.logs.Printf("connecting to database %v:%v/%v", self.conf.DatabaseHost, self.conf.DatabasePort, self.conf.DatabaseDbname)
	err := self.database.Init()
	
	if err != nil {
		self.logs.Fatal(err)
	}
	
	self.logs.Printf("running server on port %v", self.conf.Port)
	self.server.ListenAndServe()
}

func (self *Server) handleHome(
	writer http.ResponseWriter,
	req *http.Request,
) {
	outputTemplate, err := template.New("frame.html").ParseFS(
		self.resources,
		"templates/frame.html",
		"templates/pages/home.html",
	)
	
	if err != nil {
		self.logs.Print(err)
		return
	}
	
	model := HomeModel {
		FrameModel: FrameModel {
			Lang: self.defaultLang,
			Conf: self.conf,
			LoggedIn: true,
		},
	}
	
	err = outputTemplate.Execute(writer, model)
	
	if err != nil {
		self.logs.Print(err)
	}
}

func (self *Server) handleTrack(
	writer http.ResponseWriter,
	req *http.Request,
) {
	outputTemplate, err := template.New("frame.html").ParseFS(
		self.resources,
		"templates/frame.html",
		"templates/pages/track.html",
	)
	
	if err != nil {
		self.logs.Print(err)
		return
	}
	
	trackId := strings.TrimPrefix(req.URL.EscapedPath(), "/t/")
	
	reqTrack, err := self.database.GetTracks(
		backend.TrackFilter {
			Id: trackId,
		},
	)
	
	if err != nil {
		self.logs.Print(err)
		return
	}
	
	if len(reqTrack) < 1 {
		return
	}
	
	reqComments, err := self.database.GetComments(
		backend.CommentFilter {
			Track: trackId,
		},
	)
	
	if err != nil {
		self.logs.Print(err)
		return
	}
	
	reqMoreFrom, err := self.database.GetTracks(
		backend.TrackFilter {
			Author: reqTrack[0].Author,
		},
	)
	
	if err != nil {
		self.logs.Print(err)
		return
	}
	
	reqRelevantUsers, err := self.database.GetUsers(backend.UserFilter {})
	
	if err != nil {
		self.logs.Print(err)
		return
	}
	
	users := make(UserInfoMap, len(reqRelevantUsers)).
		ReadApiUserSlice(self.conf, reqRelevantUsers)
	
	model := TrackModel {
		FrameModel: FrameModel {
			Lang: self.defaultLang,
			Conf: self.conf,
			LoggedIn: true,
		},
	}.ReadApiTrack(self.conf, users, reqMoreFrom, reqComments, reqTrack[0])
	
	err = outputTemplate.Execute(writer, model)
	
	if err != nil {
		self.logs.Print(err)
	}
}
