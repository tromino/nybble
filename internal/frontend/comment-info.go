package frontend

import (
	"time"
	"codeberg.org/tromino/nybble/internal/common"
	"codeberg.org/tromino/nybble/internal/backend"
)

type CommentInfo struct {
	Body string
	Author UserInfo
	CreationTime time.Time
}

func (self CommentInfo) ReadApiComment(
	conf common.Conf,
	users UserInfoMap,
	reqComment backend.ApiComment,
) CommentInfo {
	self.Body = reqComment.Body
	self.Author = users[reqComment.Author]
	self.CreationTime = time.Unix(reqComment.CreationTime, 0)
	
	return self
}
