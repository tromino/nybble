package frontend

import (
	"codeberg.org/tromino/nybble/internal/common"
	"codeberg.org/tromino/nybble/internal/backend"
)

type TrackModel struct {
	FrameModel
	Track TrackInfo
	MoreFrom TrackInfoSlice
	Comments CommentInfoSlice
	CommentCount int64
}

func (self TrackModel) ReadApiTrack(
	conf common.Conf,
	users UserInfoMap,
	reqMoreFrom []backend.ApiTrack,
	reqComments []backend.ApiComment,
	reqTrack backend.ApiTrack,
) TrackModel {
	self.Track = self.Track.ReadApiTrack(conf, users, reqTrack)
	self.MoreFrom = self.MoreFrom.ReadApiTrackSlice(conf, users, reqMoreFrom)
	self.Comments = self.Comments.ReadApiCommentSlice(conf, users, reqComments)
	self.CommentCount = reqTrack.CommentCount
	
	return self
}
