package backend

import (
	"database/sql"
	"encoding/json"
)

type NullString sql.NullString

func (self *NullString) MarshalJSON() ([]byte, error) {
	if self.Valid {
		return json.Marshal(self.String)
	} else {
		return json.Marshal(nil)
	}
}
