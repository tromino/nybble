package backend

type CommentFilter struct {
	Id string
	Track string
	Author string
}
