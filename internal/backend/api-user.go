package backend

type ApiUser struct {
	Id string `json:"id"`
	UniqueName string `json:"uniqueName"`
	DisplayName NullString `json:"displayName"`
	Description NullString `json:"description"`
	AvatarUrl NullString `json:"avatarUrl"`
	HeaderUrl NullString `json:"headerUrl"`
	TrackCount int64 `json:"trackCount"`
	CreationTime int64 `json:"creationTime"`
}
