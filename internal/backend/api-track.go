package backend

type ApiTrack struct {
	Id string `json:"id"`
	Title string `json:"title"`
	Description NullString `json:"description"`
	ThumbnailUrl NullString `json:"thumbnailUrl"`
	AudioUrl string `json:"audioUrl"`
	Author string `json:"author"`
	CommentCount int64 `json:"commentCount"`
	CreationTime int64 `json:"creationTime"`
}
