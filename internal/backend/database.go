package backend

import (
	"log"
	"fmt"
	"time"
	"io"
	"io/fs"
	"net/http"
	"encoding/json"
	"database/sql"
	_ "github.com/lib/pq"
	"codeberg.org/tromino/nybble/internal/common"
)

type Database struct {
	ApiHandler http.Handler
	logs *log.Logger
	conf common.Conf
	resources fs.FS
	db *sql.DB
}

func NewDatabase(
	conf common.Conf,
	resources fs.FS,
	logs *log.Logger,
) *Database {
	serveMux := http.NewServeMux()
	db, err := sql.Open(
		"postgres",
		fmt.Sprintf(
			"user=%v password=%v dbname=%v host=%v port=%v sslmode=%v",
			conf.DatabaseUser,
			conf.DatabasePassword,
			conf.DatabaseDbname,
			conf.DatabaseHost,
			conf.DatabasePort,
			conf.DatabaseSslmode,
		),
	)
	
	if err != nil {
		panic(err)
	}
	
	database := &Database {
		ApiHandler: serveMux,
		logs: logs,
		conf: conf,
		resources: resources,
		db: db,
	}
	
	serveMux.HandleFunc("/api/v1/users", database.handleUsers)
	serveMux.HandleFunc("/api/v1/tracks", database.handleTracks)
	serveMux.HandleFunc("/api/v1/comments", database.handleComments)
	
	return database
}

func (self *Database) Init() error {
	err := self.db.Ping()
	
	if err != nil {
		return err
	}
	
	_, err = self.db.Query("SELECT schema_version FROM meta;")
	
	if err != nil {
		migrationFile, err := self.resources.Open("migrations/create.sql")
		
		if err != nil {
			return err
		}
		
		migrationData, err := io.ReadAll(migrationFile)
		
		if err != nil {
			return err
		}
		
		migration := string(migrationData)
		_, err = self.db.Exec(migration)
		
		if err != nil {
			return err
		}
	}
	
	_, err = self.db.Query("SELECT schema_version FROM meta;")
	
	if err != nil {
		return err
	}
	
	return nil
}

func (self *Database) GetUsers(filter UserFilter) ([]ApiUser, error) {
	users := make([]ApiUser, 0)
	
	var rows *sql.Rows
	var err error
	
	baseQuery := `
		SELECT
			id,
			unique_name,
			display_name,
			description,
			avatar_path,
			header_path,
			track_count,
			creation_time
		FROM users %v ORDER BY creation_time ASC;
	`
	
	if filter.Id != "" {
		rows, err = self.db.Query(
			fmt.Sprintf(baseQuery, "WHERE id = $1"),
			filter.Id,
		)
	} else if filter.UniqueName != "" {
		rows, err = self.db.Query(
			fmt.Sprintf(baseQuery, "WHERE unique_name = $1"),
			filter.UniqueName,
		)
	} else {
		rows, err = self.db.Query(
			fmt.Sprintf(baseQuery, ""),
		)
	}
	
	if err != nil {
		return users, err
	}
	
	for rows.Next() {
		id := new(string)
		uniqueName := new(string)
		displayName := new(sql.NullString)
		description := new(sql.NullString)
		avatarPath := new(sql.NullString)
		headerPath := new(sql.NullString)
		trackCount := new(int64)
		creationTime := new(time.Time)
		
		err := rows.Scan(
			id,
			uniqueName,
			displayName,
			description,
			avatarPath,
			headerPath,
			trackCount,
			creationTime,
		)
		
		if err != nil {
			return users, err
		}
		
		var avatarUrl NullString
		var headerUrl NullString
		
		if avatarPath.Valid {
			avatarUrl = NullString {
				String: self.conf.MediaRoot + avatarPath.String,
				Valid: true,
			}
		} else {
			avatarUrl = NullString {}
		}
		
		if headerPath.Valid {
			headerUrl = NullString {
				String: self.conf.MediaRoot + headerPath.String,
				Valid: true,
			}
		} else {
			headerUrl = NullString {}
		}
		
		users = append(users, ApiUser {
			Id: *id,
			UniqueName: *uniqueName,
			DisplayName: NullString(*displayName),
			Description: NullString(*description),
			AvatarUrl: avatarUrl,
			HeaderUrl: headerUrl,
			TrackCount: *trackCount,
			CreationTime: creationTime.Unix(),
		})
	}
	
	return users, nil
}

func (self *Database) GetTracks(filter TrackFilter) ([]ApiTrack, error) {
	tracks := make([]ApiTrack, 0)
	
	var rows *sql.Rows
	var err error
	
	baseQuery := `
		SELECT
			id,
			title,
			description,
			thumbnail_path,
			audio_path,
			author,
			comment_count,
			creation_time
		FROM tracks %v ORDER BY creation_time ASC;
	`
	
	if filter.Id != "" {
		rows, err = self.db.Query(
			fmt.Sprintf(baseQuery, "WHERE id = $1"),
			filter.Id,
		)
	} else if filter.Author != "" {
		rows, err = self.db.Query(
			fmt.Sprintf(baseQuery, "WHERE author = $1"),
			filter.Author,
		)
	} else {
		rows, err = self.db.Query(
			fmt.Sprintf(baseQuery, ""),
		)
	}
	
	if err != nil {
		return tracks, err
	}
	
	for rows.Next() {
		id := new(string)
		title := new(string)
		description := new(sql.NullString)
		thumbnailPath := new(sql.NullString)
		audioPath := new(string)
		author := new(string)
		commentCount := new(int64)
		creationTime := new(time.Time)
		
		err := rows.Scan(
			id,
			title,
			description,
			thumbnailPath,
			audioPath,
			author,
			commentCount,
			creationTime,
		)
		
		if err != nil {
			return tracks, err
		}
		
		var thumbnailUrl NullString
		
		if thumbnailPath.Valid {
			thumbnailUrl = NullString {
				String: self.conf.MediaRoot + thumbnailPath.String,
				Valid: true,
			}
		} else {
			thumbnailUrl = NullString {}
		}
		
		audioUrl := self.conf.MediaRoot + *audioPath
		
		tracks = append(tracks, ApiTrack {
			Id: *id,
			Title: *title,
			Description: NullString(*description),
			ThumbnailUrl: thumbnailUrl,
			AudioUrl: audioUrl,
			Author: *author,
			CommentCount: *commentCount,
			CreationTime: creationTime.Unix(),
		})
	}
	
	return tracks, nil
}

func (self *Database) GetComments(filter CommentFilter) ([]ApiComment, error) {
	comments := make([]ApiComment, 0)
	
	var rows *sql.Rows
	var err error
	
	baseQuery := `
		SELECT
			id,
			track,
			body,
			author,
			creation_time
		FROM comments %v ORDER BY creation_time ASC;
	`
	
	if filter.Id != "" {
		rows, err = self.db.Query(
			fmt.Sprintf(baseQuery, "WHERE id = $1"),
			filter.Id,
		)
	} else if filter.Track != "" {
		rows, err = self.db.Query(
			fmt.Sprintf(baseQuery, "WHERE track = $1"),
			filter.Track,
		)
	} else if filter.Author != "" {
		rows, err = self.db.Query(
			fmt.Sprintf(baseQuery, "WHERE author = $1"),
			filter.Author,
		)
	} else {
		rows, err = self.db.Query(
			fmt.Sprintf(baseQuery, ""),
		)
	}
	
	if err != nil {
		return comments, err
	}
	
	for rows.Next() {
		id := new(string)
		track := new(string)
		body := new(string)
		author := new(string)
		creationTime := new(time.Time)
		
		err := rows.Scan(
			id,
			track,
			body,
			author,
			creationTime,
		)
		
		if err != nil {
			return comments, err
		}
		
		comments = append(comments, ApiComment {
			Id: *id,
			Track: *track,
			Body: *body,
			Author: *author,
			CreationTime: creationTime.Unix(),
		})
	}
	
	return comments, nil
}

func (self *Database) handleUsers(
	writer http.ResponseWriter,
	req *http.Request,
) {
	query := req.URL.Query()
	
	users, err := self.GetUsers(UserFilter {
		Id: query.Get("id"),
		UniqueName: query.Get("uniqueName"),
	})
	
	if err != nil {
		self.logs.Print(err)
		return
	}
	
	value, err := json.Marshal(users)
	
	if err != nil {
		self.logs.Print(err)
		return
	}
	
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(value)
}

func (self *Database) handleTracks(
	writer http.ResponseWriter,
	req *http.Request,
) {
	query := req.URL.Query()
	
	tracks, err := self.GetTracks(TrackFilter {
		Id: query.Get("id"),
		Author: query.Get("author"),
	})
	
	if err != nil {
		self.logs.Print(err)
		return
	}
	
	value, err := json.Marshal(tracks)
	
	if err != nil {
		self.logs.Print(err)
		return
	}
	
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(value)
}

func (self *Database) handleComments(
	writer http.ResponseWriter,
	req *http.Request,
) {
	query := req.URL.Query()
	
	comments, err := self.GetComments(CommentFilter {
		Id: query.Get("id"),
		Track: query.Get("track"),
		Author: query.Get("author"),
	})
	
	if err != nil {
		self.logs.Print(err)
		return
	}
	
	value, err := json.Marshal(comments)
	
	if err != nil {
		self.logs.Print(err)
		return
	}
	
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(value)
}
