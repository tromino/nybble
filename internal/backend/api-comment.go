package backend

type ApiComment struct {
	Id string `json:"id"`
	Track string `json:"track"`
	Body string `json:"body"`
	Author string `json:"author"`
	CreationTime int64 `json:"creationTime"`
}
