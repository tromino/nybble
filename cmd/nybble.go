package main

import (
	"fmt"
	"os"
	"flag"
	"encoding/json"
	"codeberg.org/tromino/nybble/internal/frontend"
	"codeberg.org/tromino/nybble/internal/common"
	"codeberg.org/tromino/nybble/web"
)

func main() {
	args := flag.NewFlagSet("nybble", flag.ExitOnError)
	confPath := args.String("conf", "", "path to configuration file")
	args.Parse(os.Args[1:])
	
	if *confPath == "" {
		args.Usage()
		os.Exit(2)
	}
	
	confFile, err := os.ReadFile(*confPath)
	
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	
	confData := common.Conf {}
	err = json.Unmarshal(confFile, &confData)
	
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	
	server := frontend.NewServer(confData, web.Resources)
	server.Run()
}
