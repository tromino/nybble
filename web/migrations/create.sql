CREATE TABLE meta (
	schema_version INT
);

INSERT INTO meta VALUES (
	0
);

CREATE TABLE users (
	id VARCHAR(12) PRIMARY KEY,
	unique_name VARCHAR(30) NOT NULL UNIQUE,
	email VARCHAR(300) NOT NULL UNIQUE,
	pass_key BYTEA NOT NULL,
	pass_salt BYTEA NOT NULL,
	display_name VARCHAR(50),
	description VARCHAR(1000),
	avatar_path VARCHAR,
	header_path VARCHAR,
	track_count BIGINT NOT NULL,
	creation_time TIMESTAMP NOT NULL
);

CREATE TABLE tracks (
	id VARCHAR(12) PRIMARY KEY,
	title VARCHAR(100) NOT NULL,
	description VARCHAR(1000),
	thumbnail_path VARCHAR,
	audio_path VARCHAR NOT NULL,
	author VARCHAR(12) NOT NULL REFERENCES users(id),
	comment_count BIGINT NOT NULL,
	creation_time TIMESTAMP NOT NULL
);

CREATE TABLE comments (
	id VARCHAR(16) PRIMARY KEY,
	track VARCHAR(12) NOT NULL REFERENCES tracks(id),
	body VARCHAR(3000) NOT NULL,
	author VARCHAR(12) NOT NULL REFERENCES users(id),
	creation_time TIMESTAMP NOT NULL
);
